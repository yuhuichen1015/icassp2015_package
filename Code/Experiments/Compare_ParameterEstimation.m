% Copmare the parameter estimation performance for different methods:
% 1. Maximum likelihood estimator
% 2. Modified ML estimator
% 3. EM for VMF estimator

SymOperatorsSrp;
for Kappa=KappaSet
    result_file = [result_path 'VMF_FZ_K' int2str(Kappa) '_Result.mat'];
    if(exist(result_file, 'file') && ~overwrite_computation)
        warning(['Result for kappa = ' int2str(Kappa) ' exists. Computation skipped']);
        continue;
    end
    ['Computing the result for kappa = ' int2str(Kappa)]
    %%% Testing EM algorithm
    input_file = [data_path 'VMF_FZ_K' int2str(Kappa) '.mat'];
    load(input_file);
    
    Num_of_sets = size(MeanDir_All,1);
    CosinOri_All = zeros(Num_of_sets,1);
    CosinEM_All = zeros(Num_of_sets,1);
    CosinCls_All = zeros(Num_of_sets,1);
    KappaOri_All = zeros(Num_of_sets,1);
    KappaEM_All = zeros(Num_of_sets,1);
    KappaCls_All = zeros(Num_of_sets,1);
    
    for i=1:Num_of_sets
        Samples = Samples_All{i};
        %%% Estimate by the origin formula
        [Mu_est, Kappa_est] = OriginEstforVMF(Samples);
        [cs, Mu_est] = SymDistance(MeanDir_All{i}, Mu_est, Pm);
        CosinOri_All(i) = cs;
        KappaOri_All(i) = Kappa_est;
        
        %%% Estimate by the closeset angle method
        [Mu_est, Kappa_est] = CloestEstforVMF(Samples, Pm);
        [cs, Mu_est] = SymDistance(MeanDir_All{i}, Mu_est, Pm);
        CosinCls_All(i) = cs;
        KappaCls_All(i) = Kappa_est;       
        
        %%% Estimate by EM algorithm
        [Mu_est, Kappa_est]=EMforVMF(Samples, Pm);
        [cs, Mu_est] = SymDistance(MeanDir_All{i}, Mu_est, Pm);
        CosinEM_All(i) = cs;
        KappaEM_All(i) = Kappa_est;
    end
    save([result_path 'VMF_FZ_K' int2str(Kappa) '_Result.mat'],...
        'CosinOri_All', 'CosinEM_All', 'CosinCls_All',...
        'KappaOri_All', 'KappaEM_All', 'KappaCls_All');    
end
