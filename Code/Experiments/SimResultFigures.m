
%%% Show Result
Num_of_Kappa = size(KappaSet(:),1);
CosinCls_Mean = zeros(Num_of_Kappa, 1);
CosinEM_Mean = zeros(Num_of_Kappa, 1);
CosinOri_Mean = zeros(Num_of_Kappa, 1);
KappaCls_Mean = zeros(Num_of_Kappa, 1);
KappaEM_Mean = zeros(Num_of_Kappa, 1);
KappaOri_Mean = zeros(Num_of_Kappa, 1);

for k = 1:Num_of_Kappa
    Kappa = KappaSet(k);
    result_file = [result_path 'VMF_FZ_K' int2str(Kappa) '_Result.mat'];
    load(result_file);
    
    CosinOri_Mean(k) = mean(CosinOri_All);
    CosinCls_Mean(k) = mean(CosinCls_All);
    CosinEM_Mean(k) = mean(CosinEM_All);
    KappaOri_Mean(k) = mean(KappaOri_All);
    KappaCls_Mean(k) = mean(KappaCls_All);
    KappaEM_Mean(k) = mean(KappaEM_All);
end

set(0, 'DefaultAxesLineWidth', 2.0)
set(0, 'DefaultLineLineWidth', 2.0)
set(0, 'DefaultTextFontSize', 15)
set(0, 'DefaultTextFontWeight', 'bold')
set(0, 'DefaultAxesFontSize', 15)
set(0, 'DefaultAxesFontWeight', 'bold')
set(0, 'DefaultLineMarkerSize', 10)

figure;
plot(KappaSet, ones(Num_of_Kappa,1), 'r', KappaSet, CosinOri_Mean, 'b', KappaSet, CosinCls_Mean, 'g--', KappaSet, CosinEM_Mean, 'k.'), xlabel('$$\kappa_o$$', 'interpreter', 'latex'), ylabel('$$\mu^T\mu_o$$', 'interpreter', 'latex'), axis([0 max(KappaSet(:)) 0.92 1.01]);
legend('Ground Truth', 'ML for VMF', 'Modified ML for VMF', 'EM-ML for mVMF');

figure;
plot(KappaSet, KappaSet, 'r', KappaSet, KappaOri_Mean, 'b', KappaSet, KappaCls_Mean, 'g--', KappaSet, KappaEM_Mean, 'k.'), xlabel('$$\kappa_o$$', 'interpreter', 'latex'), ylabel('$$\hat{\kappa}$$', 'interpreter', 'latex');
legend('Ground Truth', 'ML for VMF', 'Modified ML for VMF', 'EM-ML for mVMF');
