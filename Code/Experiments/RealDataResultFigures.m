% Load input data and pre-computed result
load([result_path 'Kappa_Mu_Comparisons_IN100.mat']);
load([data_path 'IndexingResult_IN100.mat']);
load([data_path 'BSE_IN100_reg.mat']);

% Select the region from the original image
EA1 = SelectRegion(EA1, Selected_pts, Height, Width);
EA2 = SelectRegion(EA2, Selected_pts, Height, Width);
EA3 = SelectRegion(EA3, Selected_pts, Height, Width);
I_BSE = SelectRegion(I_BSE, Selected_pts, Height, Width);
Indexing = SelectRegion(Indexing, Selected_pts, Height, Width);
Boundary = imgIdx2Boundary(Indexing);
Kappa_ML = SelectRegion(Kappa_ML, Selected_pts, Height, Width);
Kappa_ML(Boundary==1) = 0;
Kappa_mML = SelectRegion(Kappa_mML, Selected_pts, Height, Width);
Kappa_mML(Boundary==1) = 0;
Kappa_EM = SelectRegion(Kappa_EM, Selected_pts, Height, Width);
Kappa_EM(Boundary==1) = 0;

% OEM IPF coloring image
Phi = EA1;
Phi = cat(3, Phi, EA2);
Phi = cat(3, Phi, EA3);
[Img] = imgPhi2Img(Phi, ~Boundary, 3, 1);

% Create mu image
PhiMu = zeros(Height*Width, 3);
inds = unique(Indexing(:));
for i=1:size(inds(:),1)
    [p, issig] = SpinCalc('QtoEA313', Mu_EM(inds(i),:), 0.01, 0);
    PhiMu(Indexing(:)==inds(i),:) = repmat(p*pi/180, [sum(Indexing(:)==inds(i)), 1]);
end
PhiMu = reshape(PhiMu, [Height, Width, 3]);
[ImgMu] = imgPhi2Img(PhiMu, ~Boundary, 3, 1);

% Display the results
figure; image(Img), axis image, title('Original Direction');
figure; image(ImgMu), axis image, title('Estimated Mean Direction');
figure; imagesc(Kappa_ML), axis image, colormap gray, colorbar, title('Estimated \kappa');
figure; imagesc(Kappa_mML), axis image, colormap gray, colorbar,title('Estimated \kappa');
figure; imagesc(Kappa_EM), axis image, colormap gray, colorbar,title('Estimated \kappa');
figure; imagesc(I_BSE), axis image, colormap gray, colorbar, title('BSE image');



