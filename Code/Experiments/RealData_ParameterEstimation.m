
result_file = [result_path 'Kappa_Mu_Comparisons_IN100.mat'];
if(exist(result_file, 'file') && ~overwrite_computation)
    warning(['Result file:' result_file ' exists. Computation skipped.']);
else
    % Prepare input data
    load([data_path 'IndexingResult_IN100.mat']);
    SymOperatorsSrp;
    Boundary = imgIdx2Boundary(Indexing);
    
    % Create result containers
    Num_of_grains = max(Indexing(:));
    GrainID = unique(Indexing(:));
    Kappa_ML = zeros(size(Indexing));
    Mu_ML = zeros(Num_of_grains, 4);
    PhiTrans_ML = zeros(numel(Indexing), 3);
    Kappa_mML = zeros(size(Indexing));
    Mu_mML = zeros(Num_of_grains, 4);
    PhiTrans_mML = zeros(numel(Indexing), 3);
    Kappa_EM = zeros(size(Indexing));
    Mu_EM = zeros(Num_of_grains, 4);
    PhiTrans_EM = zeros(numel(Indexing), 3);
    
    for g=1:size(GrainID(:),1)
        ['Processing grain: ' int2str(GrainID(g))]
        Region = Indexing==GrainID(g) & ~Boundary;
        Phi = [EA1(Region(:)) EA2(Region(:)) EA3(Region(:))];
        
        if(size(Phi,1)~=0)
            Phi(Phi<=0) = eps;
            [X, issig] = SpinCalc('EA313toQ', Phi*180/pi, 0.01, 0);
            
            % ML of VMF
            [mu, kappa] = OriginEstforVMF(X);
            Mu_ML(GrainID(g),:) = mu(:)';
            Kappa_ML(Region) = kappa;
            PhiTrans_ML(Region(:),:) = Phi;
            
            % Modified ML of VMF
            [mu, kappa] = CloestEstforVMF(X, Pm);
            Mu_mML(GrainID(g),:) = mu(:)';
            Kappa_mML(Region) = kappa;
            PhiTrans_mML(Region(:),:) = Phi;
            
            % EM-ML for mVMF
            [mu, kappa]=EMforVMF(X, Pm);
            Mu_EM(GrainID(g),:) = mu(:)';
            [XTrans] = TransformToRef(mu, X, Pm);
            [phiTrans, issig] = SpinCalc('QtoEA313', XTrans, 0.01, 0);
            PhiTrans_EM(Region(:),:) = phiTrans*pi/180;
            Kappa_EM(Region) = kappa;
        end
    end
    
    save(result_file,...
        'Kappa_EM', 'Mu_EM', 'PhiTrans_EM',...
        'Kappa_ML', 'Mu_ML', 'PhiTrans_ML',...
        'Kappa_mML', 'Mu_mML', 'PhiTrans_mML');
end


