% The overall simulation process. Related the Figure 1, 2 in the paper.

%% Data generation
GenerateVMFbyFZ;

%% Parameter Estimation by EMVMF
Compare_ParameterEstimation;

%% Generate the figures
SimResultFigures;
