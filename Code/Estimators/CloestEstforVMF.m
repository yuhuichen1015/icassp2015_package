function [Mu_est, Kappa_est, X] = CloestEstforVMF(Samples, Pm)
    p = size(Samples,2);
    N = size(Samples,1);
    
    X = zeros(size(Samples));
    % Transforming all angles to the be closest to the first one
    for i=1:N
        sym = zeros(24,1);  
        for j=1:24
            sym(j) = (Samples(1,:)*(Pm(:,:,j)'*Samples(i,:)'));
        end
        [cs, dd] = max(abs(sym));
        if(sym(dd)<0)
            X(i,:) = -(Pm(:,:,dd)'*Samples(i,:)')';
        else
            X(i,:) = (Pm(:,:,dd)'*Samples(i,:)')';
        end
    end
    
    Gamma = sum(X,1);
    Mu_est = Gamma / norm(Gamma,2);
    
    % Precompute the xAp, yAp for invAp
    xAp=0.001:0.1:700;
    yAp = Ap(xAp, p); 
    Kappa_est = invAp(norm(Gamma,2)/N, p, xAp, yAp);
end