function [Mu, Kappa]=EMforVMF(X, Pm, Num_of_init)
    Pm = cat(3, Pm, -Pm);
    %%% Duplicate the Euler Angles
    N = size(X,1);
    p = size(X,2);
    
    % Precompute the xAp, yAp for invAp_fast
    xAp=0.001:0.1:700;
    yAp = Ap(xAp, p);  
    
    % Initialize Mu and Kappa    
    if(nargin<3)
        Num_of_init=5;
    end
    
    Mu_All=zeros(Num_of_init, p);
    Kappa_All = zeros(Num_of_init,1);
    R_All = zeros(N,size(Pm,3),Num_of_init);
    L_All=zeros(Num_of_init,1);
    for init=1:Num_of_init        
        R = zeros(N,size(Pm,3));
        Mu = randn(1,p);
        Mu = Mu/norm(Mu,2);
        Kappa = 30;
                
        Num_of_iteration=30;
        Q=zeros(Num_of_iteration,1);
        L=zeros(Num_of_iteration,1);
        for i=1:Num_of_iteration
            % E-step
            for j=1:size(Pm,3)
                R(:,j) = VMFDensity(X, (Pm(:,:,j)*Mu')', Kappa);
            end
            Rdenom = sum(R,2);
            R = R./repmat(Rdenom, [1,size(Pm,3)]);
            
            % M-step
            tmpGamma = zeros(size(Pm,3), 4);
            for j=1:size(Pm,3)
                tmpGamma(j,:) = sum((Pm(:,:,j)'*X')'.*repmat(R(:,j), [1 4]));
            end
            Gamma = sum(tmpGamma,1);
            Mu = Gamma / norm(Gamma,2);
            Kappa = invAp(norm(Gamma,2)/N, p, xAp, yAp);
            
            % Calculate the Q and Likelihood function
            Phi = zeros(N,size(Pm,3));
            for j=1:size(Pm,3)
                Phi(:,j) = VMFDensity(X, (Pm(:,:,j)*Mu')', Kappa);
            end
            Phi = Phi./48;
            L(i) = sum(log(sum(Phi,2)));
            Q(i) = sum(sum(R.*log(Phi)));
            % update the containers
            Mu_All(init,:) = Mu;
            Kappa_All(init) = Kappa;
            R_All(:,:,init) = R;
            L_All(init)=L(i);
            if(i>=2)
                if(abs(L(i)-L(i-1))<0.05)
                    break;
                end
            end
        end      
    end
    [yy, dd] = max(L_All);
    Mu=Mu_All(dd,:);
    Kappa=Kappa_All(dd);
end