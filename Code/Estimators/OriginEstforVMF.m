function [Mu_est, Kappa_est] = OriginEstforVMF(Samples)
    p = size(Samples,2);
    N = size(Samples,1);
    
    Gamma = sum(Samples,1);
    Mu_est = Gamma / norm(Gamma,2);
    
    % Precompute the xAp, yAp for invAp
    xAp=0.001:0.1:700;
    yAp = Ap(xAp, p); 
    
    Kappa_est = invAp(norm(Gamma,2)/N, p, xAp, yAp);
end