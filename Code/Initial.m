% Define the path to the root of this package. If not, it is assumed to be
% executed in the Code directory.
package_path = [pwd '/../'];

code_path = [package_path 'Code/'];
data_path = [package_path 'Data/'];
result_path = [package_path 'Result/'];

% adding folder pathes
addpath([code_path 'DataGeneration/']);
addpath([code_path 'Estimators/']);
addpath([code_path 'Utility/']);
addpath([code_path 'SphericalDistributionsRand/']);
addpath([code_path 'Experiments/']);