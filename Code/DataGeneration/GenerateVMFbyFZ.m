% Call the script to define the cubic symmetry operators.
SymOperatorsSrp;

%%% Generate sample by fundamental zone
for Kappa = KappaSet  
    ['Generating data for kappa = ' int2str(Kappa)]
    filename = [data_path 'VMF_FZ_K' int2str(Kappa) '.mat'];
    if(exist(filename, 'file') && ~overwrite_generation)
        warning(['file:' filename ' exists. Generation skipped.']);
        continue;
    end

    StateVar_All = cell(Num_of_sets, 1);
    MeanDir_All = cell(Num_of_sets, 1);
    Samples_All = cell(Num_of_sets, 1);
    for sets = 1:Num_of_sets                   
        % Generate the main direction
        [RanSphere] = randUniformSphere(1, 4);
        MeanDir_All{sets} = RanSphere;
        
        [RandVMF] = randVMF(Num_of_samples, RanSphere, Kappa);       
        [QuatFZ, OptrIdx] = Quat2CubicFZ(RandVMF');
        
        StateVar_All{sets} = OptrIdx;
        Samples_All{sets} = QuatFZ';
    end    
    save(filename, 'Kappa', 'StateVar_All', 'MeanDir_All', 'Samples_All');
end
