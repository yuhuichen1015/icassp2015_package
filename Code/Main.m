clear;
Initial;

%%
%%% The overall simulation process. Related the Figure 1, 2 in the paper.
%%% Simulation experiment Option setting
% Overwrite the generated data if they exist.
overwrite_generation = false; 
% Overwrite the result data if they exist.
overwrite_computation = false; 
% Determine the kappa value we want to generate.
KappaSet = [1:100];
% Determine the number of sets for each kappa.
Num_of_sets = 100;
% Determine the number of samples per set.
Num_of_samples = 1000;

%%% Simulation experiment execution
SimulationExp;

%%
%%% The real data experiment process. Related the Figure 3 in the paper.
%%% Real data option setting
% Overwrite the generated data and result even if they exist.
overwrite_computation = false;

% Define the selected region to show
Selected_pts = [135, 105];
Height = 200;
Width = 200;

%%% Real data experiment execution
RealDataExp;